def separate(s):
  s_even = ""
  s_odd = ""
  for i in range(len(s)):
    if i%2 == 0:
      s_even += s[i]
    else:
      s_odd += s[i]
  print(s_even + " " + s_odd)


n = int(input())
for i in range(n):
  s = input()
  separate(s)
